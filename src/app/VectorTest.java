package app;

public class VectorTest {
    public static void main(String[] args){
        //basic setup
        Vector baseVector = new Vector(1, 2, 3),  secondVector = new Vector(3, 2, 1);
        double scalarResult, moduleResult;
        //let's sum our vectors
        Vector resultVector = baseVector.sum(secondVector);
        System.out.println("Sum of x dim should be 4.0 - " + (resultVector.getX() == 4.0));
        System.out.println("Sum of y dim should be 4.0 - " + (resultVector.getY() == 4.0));
        System.out.println("Sum of z dim should be 4.0 - " + (resultVector.getZ() == 4.0));
        System.out.println(resultVector);

        //let's find scalar product
        scalarResult = baseVector.calculateScalarProduct(secondVector);
        System.out.println("Scalar result should be 10 - " + (scalarResult == 10));

        //let's find vector product
        resultVector = baseVector.calculateVectorProduct(secondVector);
        System.out.println("Sum of x dim should be -4.0 - " + (resultVector.getX() == -4.0));
        System.out.println("Sum of y dim should be 8.0 - " + (resultVector.getY() == 8.0));
        System.out.println("Sum of z dim should be -4.0 - " + (resultVector.getZ() == -4.0));
        System.out.println(resultVector);

//        //let's find module
        moduleResult = baseVector.calculateModule();
        System.out.println("Module of two vectors should be 3.741 - " + (moduleResult > 3.74 && moduleResult < 3.742));
    }
}
