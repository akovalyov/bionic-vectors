package app;

/**
 * Sample class for vector manipulations
 */
public class Vector {
    private double x = 0.0;

    private double y;

    private double z;

    @Override
    public String toString() {
        return " r = (x:" + this.getX() + ", y:" + this.getY() + ", z:" + this.getZ() + ")";
    }

    public Vector(double x, double y, double z) {
        this.setX(x);
        this.setY(y);
        this.setZ(z);
    }

    /**
     * @return double
     */
    public double getX() {
        return this.x;
    }

    /**
     * @param x first
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * @return double
     */
    public double getY() {
        return y;
    }

    /**
     * @param y
     */
    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    /**
     * Sums some vector with current
     *
     * @param Vector vector
     * @return Vector
     */
    public Vector sum(Vector vector) {
        double newX, newY, newZ;
        newX = this.getX() + vector.getX();
        newY = this.getY() + vector.getY();
        newZ = this.getZ() + vector.getZ();
        return new Vector(newX, newY, newZ);
    }

    /**
     * Finds scalar product of current and some vector
     *
     * @param Vector vector
     * @return double
     */
    public double calculateScalarProduct(Vector vector) {
        double newX, newY, newZ;
        newX = this.getX() * vector.getX();
        newY = this.getY() * vector.getY();
        newZ = this.getZ() * vector.getZ();
        return newX + newY + newZ;
    }

    /**
     * Finds scalar product of current and some vector
     *
     * @param Vector vector
     * @return Vector
     */
    public Vector calculateVectorProduct(Vector vector) {
        double newX, newY, newZ;
        newX = this.getY() * vector.getZ() - this.getZ() * vector.getY();
        newY = this.getZ() * vector.getX() - this.getX() * vector.getZ();
        newZ = this.getX() * vector.getY() - vector.getX() * this.getY();
        return new Vector(newX, newY, newZ);
    }

    /**
     * Calculates vector's module
     *
     * @return double
     */
    public double calculateModule() {
        return Math.sqrt(Math.pow(this.getX(), 2) + Math.pow(this.getY(), 2) + Math.pow(this.getZ(), 2));
    }
}
